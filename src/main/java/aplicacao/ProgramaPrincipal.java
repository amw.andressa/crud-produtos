package aplicacao;

import entidades.Produto;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class ProgramaPrincipal {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);
        int idProduto = 1;

        List<Produto> produtos = new ArrayList<>();

        String menu = """
                |------------------------------------|
                |         MENU DE OPÇÕES             |
                |      ---------------------         |
                |  1 - Cadastrar produtos:           |
                |  2 - Listar produtos cadastrados:  |
                |  3 - Editar produtos:              |
                |  4 - Remover produtos:             |
                |  5 - Sair da aplicação             |
                |------------------------------------|
                """;

        int opcao = 0;
        while (opcao != 5) {
            System.out.println(menu);
            System.out.print("Informe sua opção: ");
            opcao = sc.nextInt();

            switch (opcao) {
                case 1:
                    System.out.println("#------------------------------------#");
                    System.out.println("Cadastrar produtos");
                    System.out.printf("Nome do produto: ");
                    sc.nextLine();
                    String nome = sc.nextLine();
                    System.out.printf("Descrição do produto: ");
                    String descricao = sc.nextLine();
                    System.out.printf("Preço do produto: ");
                    double preco = sc.nextDouble();

                    Produto produto = new Produto(idProduto, nome, descricao, preco);
                    idProduto++;
                    produtos.add(produto);
                    System.out.println("Produto cadastrado com sucesso!");
                    System.out.println("#------------------------------------#");
                    break;
                case 2:
                    System.out.println("#------------------------------------#");
                    System.out.println("Listagem de produtos cadastrados");
                    for (Produto produto1 : produtos) {
                        System.out.println(produto1);
                    }
                    System.out.println("#------------------------------------#");
                    break;
                case 3:
                    System.out.println("#------------------------------------#");
                    System.out.println("Editar produto cadastrado");
                    System.out.print("Informe id do produto que deseja editar: ");
                    int id = sc.nextInt();

                    for (Produto produto1 : produtos) {
                        if (produto1.getId() == id) {
                            System.out.printf("Novo nome: ");
                            sc.nextLine();
                            produto1.setNome(sc.nextLine());
                            System.out.printf("Nova descrição do produto: ");
                            produto1.setDescricao(sc.nextLine());
                            System.out.printf("Novo preço do produto: ");
                            produto1.setPreco(sc.nextDouble());
                        }
                    }
                    System.out.printf("Produto %s editado com sucesso!", produtos.get(id - 1).getNome(), "\n");
                    System.out.println("#------------------------------------#");
                    break;

                case 4:
                    System.out.println("#------------------------------------#");
                    try {
                        System.out.println("4 - Remover produto");
                        System.out.print("Informe id do produto que deseja remover: ");
                        id = sc.nextInt();
                        for (Produto produto1 : produtos) {
                            if (produto1.getId() == id) {
                                produtos.remove(produto1.getId() - 1);
                            }
                        }
                        System.out.println("Produto removido com sucesso!");
                        System.out.println("#------------------------------------#");
                    } catch (IndexOutOfBoundsException e) {
                        System.out.println("Erro ao remover produto. ID informado não é válido!");
                    }
                    break;
                case 5:
                    System.out.println("#------------------------------------#");
                    System.out.println("5 - Sair da aplicação");
                    System.out.println("Encerrando aplicação....");
                    System.out.println("#------------------------------------#");
            }

        }
    }
}

